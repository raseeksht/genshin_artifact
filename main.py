
import random
from pprint import pprint


class Artifact:
    def __init__(self):
        self.category = ["flower","feather","sand","goblet","circlet"]
        self.subStats = ['CRIT RATE','CRIT DMG','ATK %',"ATK",'DEF %','DEF','HP %','HP','Energy Recharge','Element Mastery']
        
        self.elementalDMGBonus = ["Hydro DMG Bonus","Cryo DMG Bonus","Pyro DMG Bonus","Electro DMG Bonus","Anemo DMG Bonus","Geo DMG Bonus"]


        self.percentStat = ['CRIT RATE','CRIT DMG','ATK %','DEF %','HP %','Energy Recharge',"Healing Bonus"]
        self.percentStat.extend(self.elementalDMGBonus)
        
        self.flatStat = ["ATK",'DEF','HP','Elemental Mastery']
        # print(percentStat)
        self.sandMainStat = self.percentStat[2:6]
        self.sandMainStat.append(self.flatStat[3])


        self.gobletMainStat = ["HP %","ATK %","DEF %","Elemental Mastery","Physical DMG Bonus"]
        self.gobletMainStat.extend(self.elementalDMGBonus)

        self.circletMainStat = self.percentStat[:5]
        self.circletMainStat.extend(['Healing Bonus',"Elemental Mastery"])

        self.artifact = {
            "mainStat":{},
            "subStats":{}
        }

        self.mainStatVal = {
            "ATK" : 47,
            "HP" : 417,
            "ATK %": 7,
            "DEF %":8.7,
            "Energy Recharge":7.8,
            "Elemental Mastery":28,
            "HP %":7.0,
            "Anemo DMG Bonus":7.0,
            "Geo DMG Bonus":7.0,
            "Cryo DMG Bonus":7.0,
            "Pyro DMG Bonus":7.0,
            "Electro DMG Bonus":7.0,
            "Hydro DMG Bonus":7.0,
            "Physical DMG Bonus":8.7,
            "Healing Bonus":5.4,
            "CRIT DMG":7.0,
            "CRIT RATE":3.5
        }


    def showStats(self):
        print(f"categories: \t{self.category}")
        print(f"subStats: \t{self.subStats}")
        print(f"elementalDMGBonus: \t{self.elementalDMGBonus}")
        print(f"percentStat: \t{self.percentStat}")
        print(f"flatStat: \t{self.flatStat}")
        print(f"sandMainStat: \t{self.sandMainStat}")
        print(f"gobletMainStat: \t{self.gobletMainStat}")
        print(f"circletMainStat: \t{self.circletMainStat}")
        # print(f"subStats: \t{self.subStats}")
        
        # elif artifactType == "sand":
    
    def getTypeOfStat(self,substat):
        # if substat in ['CRIT RATE','CRIT DMG','ATK %','DEF %','HP %','Energy Recharge']:
        if substat in self.percentStat:
            return "percent"
        elif substat in ["ATK",'DEF','HP','Elemental Mastery']:
            return "flat"
    
    def getMainStat(self,artifactType,artifact):
        if artifactType == "flower":
            mainStat = "HP"
            
        elif artifactType == "feather":
            mainStat = "ATK"
        elif artifactType == "sand":
            mainStat = random.choice(self.sandMainStat)
        elif artifactType == "goblet":
            mainStat = random.choice(self.gobletMainStat)
        elif artifactType == 'circlet':
            mainStat = random.choice(self.circletMainStat)
        # elif artifactType in ["sand","goblet","circlet"]:
        #     mainStat = random.choice()
        else:
            raise Exception(f"Unavailable Artifact Type choice: {artifactType}")
        
        
        mainStatType = self.getTypeOfStat(mainStat)
        
        
        mainStatDef = {
            "type":mainStatType,
            "value": self.mainStatVal[mainStat]
        }
        artifact['mainStat'][mainStat] = mainStatDef
        # self.artifact['mainStat'][mainStat]["type"] = mainStatType
        # self.artifact['mainStat'][mainStat]["value"] = 
        return artifact
    
    def get(self):
        self.artifact = self.getMainStat(random.choice(self.category),self.artifact)
        self.artifact = self.getSubStats(self.artifact)
        return self.artifact
    
    def getSubStats(self,artifact):
        subStatcpy = self.subStats
        numberOfSubstat = random.randint(3,4)
        for stat in range(numberOfSubstat):
            randSubstat = random.choice(subStatcpy)
            artifact['subStats'][randSubstat] = 2.1
            subStatcpy.remove(randSubstat)

        return artifact




searchFor = {
    "mainStat" : ["Cryo DMG Bonus","ATK","ATK %","Energy Recharge","Elemental Mastery","CRIT RATE","CRIT DMG"],
    "subStats":["CRIT RATE","CRIT DMG"]
}


counter = 0
crits = 0
runs = 40
for i in range(runs):
    new = Artifact()

    a = new.get()
    # print(a['subStats'].keys())
    # pprint(a,indent=4)

    if list(a['mainStat'].keys())[0] in searchFor['mainStat']:
        # for wantedSubstat in searchFor['subStats']:
        for each in list(a['subStats'].keys()):
            if "CRIT" in each and len(a['subStats'].keys()) < 4:
                crits +=1 
                break
        if set(searchFor['subStats']).intersection(set(a['subStats'].keys())) == set(searchFor['subStats']):
            mainstat = list(a['mainStat'].keys())[0]
            substats = list(a['subStats'].keys())

            # pprint(a)
            # print(forPrint)
            print(f"mainstat : {mainstat}")
            print(f"substats : {substats}")

            print()
            counter +=1

print(f"{counter} desired artifact found after {runs} domain worth {runs * 40} resin")
print(f"{crits} artifact found with crit Dmg/rate as substat")

